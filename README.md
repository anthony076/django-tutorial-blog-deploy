# Django-totorial-blog-deploy

This repo is for deploy django project to Heroku, official tutorial please refer to :
<https://devcenter.heroku.com/articles/getting-started-with-python#introduction>

Usage:

	1. git clone https://github.com/chinghua0731/django-tutorial-blog-deploy.git
	2. heroku create
	3. git init
	4. heroku git:remote -a <heroku-git-position>
	5. git add .
	6. git commit -m "Message"
	7. git push heroku master
	8. heroku open


